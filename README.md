# face

#### 项目介绍
安卓人脸识别闸机，基于android 5.1   虹软sdk 开发

下载试用地址：https://www.pgyer.com/DN4e

#### 软件架构
- 开发平台：windows10 + android studio + android 5.1
- 测试手机：huawei 7x + 前置相机

主要使用的插件：
- 日志：com.orhanobut:logger:2.2.0
- 相机：io.fotoapparat:fotoapparat:2.4.0
- 授权：com.github.tbruyelle:rxpermissions:0.10.2
- 列表刷新分页：com.github.CymChad:BaseRecyclerViewAdapterHelper:2.9.30'   'com.lcodecorex:tkrefreshlayout:1.0.7'
- 数据存储："com.github.Raizlabs.DBFlow:dbflow-core:4.2.4"

#### 安装教程

1. git clone https://gitee.com/panmingzhi/face.git
2. 使用android studio 打开该项目
3. 也可以直接下载试用：https://www.pgyer.com/DN4e


#### app截图：
- ![输入图片说明](https://images.gitee.com/uploads/images/2018/1009/140759_7da91030_87848.jpeg "微信图片_20181009140117.jpg")
- ![输入图片说明](https://images.gitee.com/uploads/images/2018/1009/140808_d15f5ccb_87848.jpeg "微信图片_20181009140110.jpg")
- ![输入图片说明](https://images.gitee.com/uploads/images/2018/1009/140814_43e725b5_87848.jpeg "微信图片_20181009140113.jpg")
- ![输入图片说明](https://images.gitee.com/uploads/images/2018/1009/140821_7341bb63_87848.jpeg "微信图片_20181009140106.jpg")
