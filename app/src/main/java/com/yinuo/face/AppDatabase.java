package com.yinuo.face;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by panmingzhi on 2018/10/4.
 */

@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
public class AppDatabase {

    public static final String NAME = "AppDatabase";

    public static final int VERSION = 1;
}
